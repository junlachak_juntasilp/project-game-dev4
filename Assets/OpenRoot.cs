using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenRoot : MonoBehaviour
{
    public Animator isOpenbox;

    public GameObject rootItem;

    public Transform itemPosition;
    // Start is called before the first frame update
    void Start()
    {
        isOpenbox.SetBool("isOpenBox", false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        
        if (other.transform.tag == "Player")
        { 
            isOpenbox.SetBool("isOpenBox", true); 
            StartCoroutine(Openbox());
        }
    }

    IEnumerator Openbox()
    {
        yield return new WaitForSeconds(1.5f);

       // GameObject itemPre  =
       Instantiate(rootItem, itemPosition.position,Quaternion.identity);
            
        Destroy(gameObject);
    }
}
