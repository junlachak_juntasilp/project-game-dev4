using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityStandardAssets.Characters.FirstPerson;

public class PauseGame : MonoBehaviour
{
    public bool isPaused = false;

    public GameObject player;

    public GameObject canvasOption;
    
    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Cancel"))
        {
            if (isPaused == false)
            {
                Time.timeScale = 0;
                isPaused = true;
                canvasOption.SetActive(true);
                player.GetComponent<FirstPersonController>().enabled = false;
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
            }
            else
            {
                canvasOption.SetActive(false);
                player.GetComponent<FirstPersonController>().enabled = true;
                isPaused = false;
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;
                Time.timeScale = 1;
            }
        }
    }

    public void Option()
    {
        SceneManager.LoadScene("SceneOptions", LoadSceneMode.Additive);
    }

    public void ReGame()
    {
        canvasOption.SetActive(false);
        player.GetComponent<FirstPersonController>().enabled = true;
        isPaused = false;
        Time.timeScale = 1;
        SceneManager.LoadScene("Mapscene");
    }

    public void ReturnToMainMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void Exit()
    {
        Application.Quit();
    }
}
