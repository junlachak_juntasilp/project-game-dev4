using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dropitem : MonoBehaviour
{
    public GameObject item;

    [SerializeField] GameObject setfalse; 
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.collider.tag == "Bullet")
        {
            setfalse.SetActive(false);
            item.SetActive(true);
        }
    }
}
