using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace UnityStandardAssets.Characters.FirstPerson
{
    public class WinGame : MonoBehaviour
    {
        public GameObject canvasWin;
        public GameObject player;
        private void OnTriggerEnter(Collider other)
        {
            if (other.transform.tag == "Player")
            {
                
                canvasWin.SetActive(true);
                StartCoroutine(winGame());

            }
        }

        IEnumerator winGame()
        {
            yield return new WaitForSeconds(0.5f);
            Time.timeScale = 0;
            player.GetComponent<FirstPersonController>().enabled = false;
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;

        }   
    }

}
