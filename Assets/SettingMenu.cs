using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using  UnityEngine.UI;
public class SettingMenu : MonoBehaviour
{
    public AudioMixer audioMixer;
    public AudioMixer VFX;


    public void SetVolumeBG(float volume)
    {
        audioMixer.SetFloat("volume", volume);
    }


    

    public void SetFullScreen(bool isFullscreen)
    {
        Screen.fullScreen = isFullscreen;
    }
}
