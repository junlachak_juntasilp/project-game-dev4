using UnityEngine;

[CreateAssetMenu(fileName = "New Charecter", menuName = "Characters")]
public class CharacterSprite : ScriptableObject
{
  public string fullname;
  public Sprite portrait;

}
