using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class candybomb : MonoBehaviour
{
    public float delay = 3f;
    public float radious = 5f;
    public float force = 700f; 
    private float countdown;
    private bool hasExploded = false;
    public  float candyDm ;

    public GameObject explosionEffect;

    private void Start()
    {
        countdown = delay;
    }

    private void Update()
    {
        countdown -= Time.deltaTime;
        if (countdown <= 0 && !hasExploded)
        {
            Explode();
            hasExploded = true;
        }
    }

    private void OnCollisionStay(Collision other)
    {
        if (other.transform.tag == "bugs")
        {
            Enemyhealth enemyhealth = other.transform.GetComponent<Enemyhealth>();
            enemyhealth.candyDamage(candyDm);
        }
    }

    void Explode()
     {
         Instantiate(explosionEffect, transform.position, transform.rotation);
         Collider[] colliederTodestroy = Physics.OverlapSphere(transform.position, radious);
        
         foreach (Collider nearbyObject in colliederTodestroy)
         {
            
             Destructible dest = nearbyObject.GetComponent<Destructible>();
             if (dest != null)
             {
                
                 dest.Destroy();
                
             }
            
         }
         Collider[] collidersToMove = Physics.OverlapSphere(transform.position, radious);
        
         foreach (Collider nearbyObject  in collidersToMove)
         {
            
             Rigidbody rb = nearbyObject.GetComponent<Rigidbody>();
             if (rb != null)
             {
                 rb.AddExplosionForce(force, transform.position, radious);
               
             }
            
         }
         Destroy(gameObject);
     }
}
