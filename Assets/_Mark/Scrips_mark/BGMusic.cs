using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGMusic : MonoBehaviour
{
    private static BGMusic BackgroundMusic;

    private void Awake()
    {
        if (BackgroundMusic == null)
        {
            BackgroundMusic = this;
            DontDestroyOnLoad(BackgroundMusic);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    
}
