using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BombCooldown : MonoBehaviour
{
    public float cooldownCandy ;

    public Image candybomnImage;

    bool isCooldown = false;

   // public KeyCode Candybomb;
    // Start is called before the first frame update
    void Start()
    {
        candybomnImage.fillAmount = 0;
    }

    // Update is called once per frame
    void Update()
    {
        candyBomb();
    }

    void candyBomb()
    {
        if (Input.GetKeyDown(KeyCode.Q) &&  isCooldown == false)
        {
            isCooldown = true;
            candybomnImage.fillAmount = 1;
        }

        if (isCooldown)
        {
            candybomnImage.fillAmount -= 1 / cooldownCandy * Time.deltaTime;
            if (candybomnImage.fillAmount <=0)
            {
                candybomnImage.fillAmount = 0;
                isCooldown = false;
            }
        }
    }
}
