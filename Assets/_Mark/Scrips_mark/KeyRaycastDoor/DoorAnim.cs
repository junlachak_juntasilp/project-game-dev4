using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorAnim : MonoBehaviour
{
   public Animator hingeWhere;
   public static bool open_door;
   public string indexDoor;

   private void Start()
   {
       open_door = false;
   }

   private void OnTriggerStay(Collider other)
   {
       if (other.transform.tag == "Player" )
       {
           if (Input.GetKey(KeyCode.E) ) 
                   hingeWhere.SetBool("isOpen", true); 
           open_door = true;
       }
      
   }
}
