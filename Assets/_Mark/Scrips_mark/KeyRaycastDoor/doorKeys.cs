using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class doorKeys : MonoBehaviour
{
    public bool inTrigger;
    public static bool hasKey ;
    public string  indexKeys;

    public GameObject keygone;
    public Component doorToOpen;

    private void Start()
    {
        hasKey = false;
    }


    public void OnTriggerStay(Collider other)
    {
        inTrigger = true;
        if (Input.GetKeyDown(KeyCode.E) )
            doorToOpen.GetComponent<BoxCollider>().enabled = true;
        hasKey = true;
        


        //  DoorScrips.hasKey = true;
        if (Input.GetKeyDown(KeyCode.E))
            keygone.SetActive(false);
        
    }
    

    private void OnTriggerExit(Collider other)
    {
        inTrigger = false;
    }

    private void OnGUI()
    {
        if (inTrigger)
        {
            GUI.Box(new Rect(0,60,200,25), "Press [E] to take key");
        }

    }

    
  
}
