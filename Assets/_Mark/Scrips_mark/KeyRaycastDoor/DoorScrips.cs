using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorScrips : MonoBehaviour
{
    public static bool doorKey;

    public static bool open;
    public static bool close;
    public static bool isTrigger;
    public static bool hasKey;
    public GameObject _key;


    private void Start()
    {
        if (_key.activeSelf)
        {
            hasKey = false;
        }
        else
        {
            hasKey = true;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        isTrigger = true;
    }

    private void OnTriggerExit(Collider other)
    {
        isTrigger = false;
    }

    void Update()
    {
        if (open && hasKey == true)
        {
            var newRot = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(0.0f, -90.0f, 0.0f),
                Time.deltaTime * 100);
            transform.rotation = newRot;
        }
        

        if (isTrigger)
        {
            if (close)
            {
                if (doorKey && hasKey == true)
                {
                    if (Input.GetKeyDown(KeyCode.E))
                    {
                        open = true;
                        close = false;
                    }
                }
            }
            else
            {
                if (Input.GetKeyDown(KeyCode.E))
                {
                    close = true;
                    open = false;
                }
            }
        }
    }

    private void OnGUI()
    {
        /*  if (isTrigger)
          {
              if (open)
              {
                  GUI.Box(new Rect(0, 0, 200, 25), "Press [E] to close");
              }
  
              else
              {
                  if (doorKey)
                  {
                      GUI.Box(new Rect(0, 0, 200, 25), "Press [E] to open");
                  }
                  else
                  {
                      GUI.Box(new Rect(0, 0, 200, 25), "Need a Key");
                  }
              }*/
    }
}



