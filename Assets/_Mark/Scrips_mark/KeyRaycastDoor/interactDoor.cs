using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class interactDoor : MonoBehaviour
{
     public GameObject canvasOpenDoorHasKey;
     public GameObject canvasOpenDoorNoKey;
     public GameObject LastdoorToOpen;
     public Component turnoffbox;


     private void OnTriggerStay(Collider other)
     {
          if (other.transform.tag == "Player" && DoorAnim.open_door && doorKeys.hasKey )
          {
               if (!canvasOpenDoorHasKey.activeSelf)
               {
                    canvasOpenDoorNoKey.SetActive(true);
               }
          }
          if (!DoorAnim.open_door && !doorKeys.hasKey)
          {
               canvasOpenDoorHasKey.SetActive(true);
               canvasOpenDoorNoKey.SetActive(false);
          }
          
          
          if (triggerPoint.isPass)
          {
               canvasOpenDoorHasKey.SetActive(false);
               canvasOpenDoorNoKey.SetActive(false);
               turnoffbox.GetComponent<BoxCollider>().enabled = false;
          }

     }

     private void OnTriggerExit(Collider other)
     {
          canvasOpenDoorHasKey.SetActive(false);
          canvasOpenDoorNoKey.SetActive(false);
     }
}
