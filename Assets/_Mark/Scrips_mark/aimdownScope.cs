using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class aimdownScope : MonoBehaviour
{
    public Vector3 aimDownSight;
// 0.01,0.84,0.77
    public Vector3 HipFire;
    public float aimspeed = 40;
    
    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            transform.localPosition = Vector3.Slerp(transform.localPosition, aimDownSight,aimspeed * Time.deltaTime);
        }

        if (Input.GetMouseButtonUp(1))
        {
            transform.localPosition = HipFire;
        }
    }
}
