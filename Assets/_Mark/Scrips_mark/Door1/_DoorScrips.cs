using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class _DoorScrips : MonoBehaviour
{
   public int index = -1;
   public bool open = false;
 

   public void ChangeDoorState()
   {
      open = !open; 
   }

   private void Update()
   {
      if (open)
      {
         var newRot = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(90.0f, 0f, -50.0f),
            Time.deltaTime * 200);
         transform.rotation = newRot;
      }
      else
      {
         var newRot = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(90.0f, 0.0f, 0.0f),
            Time.deltaTime * 200);
         transform.rotation = newRot;
      }
   }
}
