using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInteraction : MonoBehaviour
{
    public GameObject currentObj;

    public InteractObject currentInterObjScrips = null;

    public Inventory Inventory;

    public GameObject canvasUnlock;
    public GameObject canvasCantUnlock;

    public AudioSource OpenAudio;
    public AudioSource LockedAudio;

    // Update is called once per frame
    void Update()
    {
        
        if (Input.GetKeyDown(KeyCode.F) && currentObj )
        {
            if (currentInterObjScrips.inventory)
            {
                Inventory.AddItem(currentObj);
                currentObj = null;
            }
            
            //door
            if (Input.GetKeyDown(KeyCode.F))
            {
                if (currentInterObjScrips.openable)
                {
                    if (currentInterObjScrips.locked)
                    {
                        if (Inventory.FindItem(currentInterObjScrips.itemNeeded))
                        {
                            currentInterObjScrips.locked = false;
                            Debug.Log(currentObj.name+ "was unlocked");
                            Destroy(Inventory.inventory[0]);
                            canvasUnlock.SetActive(true);
                            LockedAudio.Play();
                        }
                        else
                        {
                            Debug.Log(currentObj.name + "was not unlocked");
                            canvasCantUnlock.SetActive(true);
                            StartCoroutine(ExitDoorLock());
                            LockedAudio.Play();
                        }
                    }
                    else
                    {
                        GameObject keyOpen = Inventory.FindItemByType("Key_OPENDOOR");
                        Inventory.RemoveItem(keyOpen);
                        Debug.Log(currentObj.name+"is unlocked");
                        currentInterObjScrips.open();
                        StartCoroutine(ExitDoorOpen());
                        OpenAudio.Play();

                    }
                }
                
            }
            
        }
        
        //use Potion
        if (Input.GetKeyDown(KeyCode.E) && HealthSystem.CurrenthealthPlayer < 20000)
        {
            GameObject potion = Inventory.FindItemByType("Health Potion");
            if (potion != null)
            {
                HealthSystem.CurrenthealthPlayer += UpHp.plushp;
                Inventory.RemoveItem(potion);
                Debug.Log("Use Potion");
            } 
            
        }
        
        
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("interactobject")){}
        {
            Debug.Log(other.name);
            currentObj = other.gameObject;
            currentInterObjScrips = currentObj.GetComponent<InteractObject>();
        }
    }

    IEnumerator ExitDoorLock()
    {
        yield return new WaitForSeconds(1f);
        canvasCantUnlock.SetActive(false);

    }
    IEnumerator ExitDoorOpen()
    {
        yield return new WaitForSeconds(1f);
        canvasUnlock.SetActive(false);

    }
    
   
}
