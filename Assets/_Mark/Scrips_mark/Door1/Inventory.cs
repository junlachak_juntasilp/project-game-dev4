using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour
{
    public GameObject[] inventory = new GameObject[10];
    public Button[] inventoryImage = new Button[6];

    public void AddItem(GameObject item)
    {
        bool itemAdded = false;
        for (int i = 0; i < inventory.Length; i++)
        {
            if (inventory[i] == null)
            {
                inventory[i] = null;
                inventoryImage[i].image.overrideSprite = item.GetComponent<SpriteRenderer>().sprite;
                inventory[i] = item;
                Debug.Log(item.name + "was added");
                itemAdded = true;
                item.SendMessage("DoInteraction");
                
                break;
            }
        }

        if (!itemAdded)
        {
            Debug.Log("Inventory Full - Item Not Added");
        }
    }

    public bool FindItem(GameObject item)
    {
        for (int i = 0; i < inventory.Length; i++)
        {
            if (inventory[i] == item)
            {
                return true;
            }
        }

        return false;
    }

    public GameObject FindItemByType(string itemType)
    {
        for (int i = 0; i < inventory.Length; i++)
        {
            if (inventory[i] != null)
            {
                if (inventory[i].GetComponent<InteractObject>().itemType == itemType)
                {
                    return inventory[i];
                }
            }
        }
        return null;
    }
    public void RemoveItem(GameObject item)
    {
        for (int i = 0; i < inventory.Length; i++)
        {
            if (inventory[i] == item) 
            {
                inventory[i] = null;
                inventoryImage[i].image.overrideSprite = null;
                break;
            }
        }
    }
}
