using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractObject : MonoBehaviour
{
    public bool inventory; 
    public bool openable;
    public bool locked;
    public string itemType;
    public GameObject itemNeeded;
    
   // public GameObject canvasPressE;
    

    public Animator anim;
    public void DoInteraction()
    {
        gameObject.SetActive(false);

        

    }

    public void open()
    {
        anim.SetBool("open", true);
    }

  /*  private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Player")
        {
            canvasPressE.SetActive(true);
        }
        
    }

    private void OnTriggerExit(Collider other)
    {
        canvasPressE.SetActive(false);
    }*/
}
