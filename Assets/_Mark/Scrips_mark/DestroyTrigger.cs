using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Object = System.Object;

public class DestroyTrigger : MonoBehaviour
{
   
   public Component triggerFalse;
   public GameObject Canvasfalse;
   
   private void OnTriggerEnter(Collider other)
   {
      triggerFalse.GetComponent<BoxCollider>().enabled = false;

      if (Input.GetKeyDown(KeyCode.F))
      {
         Canvasfalse.SetActive(false);

      }
   }
}
