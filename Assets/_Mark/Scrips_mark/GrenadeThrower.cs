using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrenadeThrower : MonoBehaviour
{
    // Start is called before the first frame update
    public float throwForce = 40f;
    public GameObject grenadePrefeb;
    public float cooldownTime;
    private float NextBombTime;

    // Update is called once per frame
    void Update()
    {
        if (Time.time > NextBombTime)
        {
            if (Input.GetKeyDown(KeyCode.Q))
            {
                NextBombTime = Time.time + cooldownTime;
                ThrowGrenade();
            }
        }
        
    }

     void ThrowGrenade()
    {
     GameObject grenade =  Instantiate(grenadePrefeb, transform.position, transform.rotation);
     Rigidbody rb = grenade.GetComponent<Rigidbody>();
     rb.AddForce(transform.forward * throwForce , ForceMode.VelocityChange);
    }
}
