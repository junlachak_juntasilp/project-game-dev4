using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destructible : MonoBehaviour
{
    public GameObject destroyVersion;

    public void Destroy()
    {
        Instantiate(destroyVersion, transform.position, transform.rotation);
        
    }
}
