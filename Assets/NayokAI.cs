using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityStandardAssets.Cameras;
using Random = UnityEngine.Random;

public class NayokAI : MonoBehaviour
{
    public NavMeshAgent agent;

    public Transform player;
    public GameObject projectile;
    public Transform shoots;
    public LayerMask whatIsGround, whatIsPlayer;

    public Animator anim;
    //Patrolling
    public Vector3 walkPoint;

    private bool walkPointSet;

    public float walkPointRange;
    
    
    //attacking
    public float timeBetweenAttacks;

    private bool alreadyAttacted;
    
    //states
    public float sightRange, attackRange;
    public bool playerInSightRange, playerInAttackRange;


    private void Awake()
    {
        player = GameObject.Find("FPSController Finished").transform;
        agent = GetComponent<NavMeshAgent>();
    }

    private void Update()
    {
        
        playerInSightRange = Physics.CheckSphere(transform.position, sightRange, whatIsPlayer);
        playerInAttackRange = Physics.CheckSphere(transform.position, attackRange, whatIsPlayer);
        if (!playerInSightRange && !playerInAttackRange)
        {
            Patrolling();
        }

        if (playerInSightRange && !playerInAttackRange)
        {
            ChasePlayer();
        }

        if (playerInSightRange && playerInAttackRange)
        {
            AttackPlayer();
        }
    }

    void Patrolling()
    {
        anim.SetBool("isChasing" , true);
        if (!walkPointSet)
        {
            SearchWalkPoint();
        }

        if (walkPointSet)
        {
            agent.SetDestination(walkPoint);
        }

        Vector3 distanceToWalkPoint = transform.position - walkPoint;

        if (distanceToWalkPoint.magnitude <1f)
        {
            walkPointSet = false;
        }
    }

    void SearchWalkPoint()
    {
        float randomZ = Random.Range(-walkPointRange, walkPointRange);
        float randomx = Random.Range(-walkPointRange, walkPointRange);

        walkPoint = new Vector3(transform.position.x + randomx, transform.position.y, transform.position.z+ randomZ);
        if (Physics.Raycast(walkPoint, -transform.up, 2f, whatIsGround)) ;
        {
            walkPointSet = true;
        }
    }
    void ChasePlayer()
    {
        agent.SetDestination(player.position);
    }
    void AttackPlayer()
    {
        anim.SetBool("isAttacting", true);
        agent.SetDestination(player.position);
        
        transform.LookAt(player);
        if (!alreadyAttacted)
        {
            Vector3 playerPos = new Vector3(player.position.x, player.position.y + 1, player.position.z);
            //attack
            Rigidbody rb = Instantiate(projectile, shoots.position, Quaternion.identity).GetComponent<Rigidbody>();
            rb.AddForce(transform.forward * 40f,ForceMode.Impulse );
            rb.AddForce(transform.up * 7f,ForceMode.Impulse);
            rb.transform.rotation = Quaternion.LookRotation(playerPos);

            //
            alreadyAttacted = true;
            Invoke(nameof(ResetAttack), timeBetweenAttacks);
        }
    }

    void ResetAttack()
    {
        alreadyAttacted = false;
    }

    public void TakeDamage(float damage)
    {
        
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, attackRange);
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, sightRange);
    }
}
