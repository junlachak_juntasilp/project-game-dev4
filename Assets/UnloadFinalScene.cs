using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UnloadFinalScene : MonoBehaviour
{
    private void OnEnable()
    {
        SceneManager.UnloadSceneAsync(4);
    }
}
