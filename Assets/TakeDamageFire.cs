using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakeDamageFire : MonoBehaviour
{
    private float damageFire = 1;
    
    private void OnTriggerStay(Collider other)
    {
        HealthSystem.Damaged(damageFire);
        
    }
}
