using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class ShootProjectile : MonoBehaviour
{
    public Camera cam;
    private Vector3 destination;

    public GameObject projectile;

    public Transform LHJellyPoint, RHJellyPoint;

    private bool LeftHand;

    public float projectilespeed = 30;

    private float timeToJelly;
    public float JellyRate = 1;

    public float arcRange =1;
    

    
    //animate
    private Animator anim;
    private int iswalkingHash;
    private int isRunningHash;
    private int isJumpingHash;
    
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        iswalkingHash = Animator.StringToHash("isWalking");
        isRunningHash = Animator.StringToHash("isRunning");
        isJumpingHash = Animator.StringToHash("isGrounded");
    }

    // Update is called once per frame
    void Update()
    {
        
        bool forwardPressed = Input.GetKey("w");
        bool runPressed = Input.GetKey("left shift");
        bool jumpPressed = Input.GetKey("space");
        bool isWalking = anim.GetBool(iswalkingHash);
        bool isRunning = anim.GetBool(isRunningHash);
        bool isGrounded = anim.GetBool(isJumpingHash);


        
 //shooting jelly       
        if (Input.GetButton("Fire1") && Time.time >= timeToJelly)
        {
            timeToJelly = Time.time + 1 / JellyRate;
            anim.SetBool("shooting", true);
            shootJelly();
        }
        else
        {
            anim.SetBool("shooting", false);

        }
//walking animate
        if (!isWalking &&forwardPressed)
        {
            anim.SetBool(iswalkingHash, true);

        }
        if (isWalking && !forwardPressed)
        {
            anim.SetBool(iswalkingHash, false);

        } 
       


        
//running animate
        /*if (!isRunning &&forwardPressed && runPressed )
        {
            anim.SetBool(isRunningHash, true);

        }
        if (isRunning &&!forwardPressed && !runPressed )
        {
            anim.SetBool(isRunningHash, false);

        } */
//jump animate
        if (isGrounded  && jumpPressed)
        {
            anim.SetBool(isJumpingHash, false);

        }

        if (!isGrounded && !jumpPressed)
        {
            anim.SetBool(isJumpingHash, true);
        }
        
    }

    private void FixedUpdate()
    {
        AnimatorStateInfo info = anim.GetCurrentAnimatorStateInfo(0);
        if (info.IsName("shooting")) anim.SetBool("shooting",false);
    }

    void shootJelly()
    {
        Ray ray = cam.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
        RaycastHit hit;
        if(Physics.Raycast(ray, out hit))
            destination = hit.point;
        else 
            destination = ray.GetPoint(1000);
        if (LeftHand)
        {
            LeftHand = false;
            InstaceProjectile(LHJellyPoint);

        }
        else
        {
            LeftHand = true;
            InstaceProjectile(RHJellyPoint);

        }
       
        
       
        // anim.CrossFadeInFixedTime("shooting",0.1f);
    }

    void InstaceProjectile(Transform jellyPoint)
    {
        var projectileObj = Instantiate(projectile, jellyPoint.position, Quaternion.identity) as GameObject;
        projectileObj.GetComponent<Rigidbody>().velocity = (destination - jellyPoint.position).normalized * projectilespeed;

        iTween.PunchPosition(projectileObj,new Vector3(Random.Range(-arcRange, arcRange),Random.Range(-arcRange, arcRange),0),Random.Range(0.5f,2));
}
}
