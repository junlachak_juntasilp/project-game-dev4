using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class HealthSystem : MonoBehaviour
{
  
    public static float CurrenthealthPlayer ;
    public float HealthPlayer;
    
    

    public GameObject healthBarUIPlayer;
    public GameObject GameOverUI;
   // public Component playerController;

    public Slider _sliderPlayer;
    public TextMeshProUGUI HPplayerTxt;

    public GameObject player;
    
    
   
    public void Start()
    {
        CurrenthealthPlayer = HealthPlayer;
        _sliderPlayer.value = CalculateHealthPlayer();
    }

    void Update()
    {
        _sliderPlayer.value = CalculateHealthPlayer();
        if (CurrenthealthPlayer < HealthPlayer)
        {
            healthBarUIPlayer.SetActive(true);
        }

        if (CurrenthealthPlayer <= 0)
        {
            die();
        }

        if (CurrenthealthPlayer > HealthPlayer)
        {
            CurrenthealthPlayer = HealthPlayer;
        }

        HPplayerTxt.text =  ""+ CurrenthealthPlayer;
        
    }

    float CalculateHealthPlayer()
    {
        return CurrenthealthPlayer / HealthPlayer;
    }
    
    
    public  static void Damaged(float damageEnemy)
    {
        CurrenthealthPlayer -= damageEnemy;

    }

    public void PlusHP(float pHp)
    {
        HealthPlayer += pHp;
    }

    void die()
    {
        GameOverUI.SetActive(true);
        StartCoroutine(LoseGame());
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag== "HPheal" ) //&& CurrentHealthPlayer < )
        {
            HealthPlayer += 10;
        }
    }
    IEnumerator LoseGame()
    {
        yield return new WaitForSeconds(0.5f);
        Time.timeScale = 0;
        player.GetComponent<FirstPersonController>().enabled = false;
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;

    }   
}


