using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadIntroFinal : MonoBehaviour
{
    public GameObject HpBoss;
    public GameObject soundFinal;
    public GameObject stopSound;

   
    private void OnTriggerEnter(Collider other)
    {
        soundFinal.SetActive(true);
        stopSound.SetActive(false);
        HpBoss.SetActive(true);
        SceneManager.LoadScene(4, LoadSceneMode.Additive);
        
    }

    private void OnTriggerExit(Collider other)
    {
        Destroy(gameObject);
    }
}
