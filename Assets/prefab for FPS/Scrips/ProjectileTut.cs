using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileTut : MonoBehaviour
{
   private bool collided;
   public GameObject impactVFX;
   private ATKall _atKall;



   

   private void OnCollisionEnter(Collision co)
   {
      if(co.collider.gameObject.tag != "Bullet" && co.gameObject.tag != "Player" && !collided)
      {
         collided = true;
         var impact = Instantiate(impactVFX, co.contacts[0].point, Quaternion.identity) as GameObject;
         Destroy(impact,2);
         Destroy(gameObject);
         
      }

      if (co.collider.transform.tag == "bugs")
      {
         Enemyhealth enemyhealthScrips = co.transform.GetComponent<Enemyhealth>();
         enemyhealthScrips.DeductHealth(ATKall.atkDamage);

      }
     
   }

   private void Update()
   {
      
      Destroy(gameObject, 0.8f);
   }

}
