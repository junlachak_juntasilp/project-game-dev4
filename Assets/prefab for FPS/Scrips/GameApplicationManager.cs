using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameApplicationManager : MonoBehaviour
{
    static protected GameApplicationManager singletonInstance = null;

    static public GameApplicationManager Instance
    {
        get
        {
            if (singletonInstance == null)
            {
                singletonInstance = GameObject.FindObjectOfType<GameApplicationManager>();
                GameObject container = new GameObject("GameApplicationManager");
                singletonInstance = container.AddComponent <GameApplicationManager >();
            }
            return singletonInstance;
        }
        
    }
    public bool IsOptionMenuActive 
    {
         get { return isOptionMenuActive; }
         set { isOptionMenuActive = value; } 
    }
    protected bool isOptionMenuActive = false;

    void Awake()
    {
        if (singletonInstance == null)
        {
            singletonInstance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            if (this != singletonInstance)
            {
                Destroy(this.gameObject);
            }
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
