using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShoot : MonoBehaviour
{
    public GameObject projectile;
    public Transform projectilePoint;
    
    public  void shoot()
    {
        Rigidbody rb = Instantiate(projectile, projectilePoint.position, 
            Quaternion.identity).GetComponent<Rigidbody>();
        rb.AddForce(transform.forward *30f,ForceMode.Impulse);
        rb.AddForce(transform.up * 7f, ForceMode.Impulse);
    }
   
    
}
