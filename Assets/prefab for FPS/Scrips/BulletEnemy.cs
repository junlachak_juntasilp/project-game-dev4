using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletEnemy : MonoBehaviour
{ 
    public  float damageEnemy;
    public GameObject impactEffect;

    

    private void OnTriggerEnter(Collider other)
    {
        
        GameObject impact = Instantiate(impactEffect, transform.position, Quaternion.identity);
        Destroy(impact, 2);
        if (other.transform.tag == "Player")
        {
           
            HealthSystem.Damaged(damageEnemy);

        }
    }
}
