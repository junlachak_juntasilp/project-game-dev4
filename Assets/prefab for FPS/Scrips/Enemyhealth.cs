using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Enemyhealth : MonoBehaviour
{
    public float health;
    private float maxHealth;
    public GameObject healthBarUI;
    public Slider _slider;
    public Animator anim;


    public static Enemyhealth intance;

    private void Awake()
    {
        intance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        maxHealth = health;
        _slider.value = CalculateHealth();
        //bulletSpawn = GameObject.Find("gun/bulletSpawn");
    }

    // Update is called once per frame
    void Update()
    {
        _slider.value = CalculateHealth();

        if (maxHealth < health)
        {
            healthBarUI.SetActive(true);
            
        }

        if (health <= 0)
        {
            
            Dead();
          
        }
        

        if (health > maxHealth)
        {
            health = maxHealth;
        }
        
        
    }

    float CalculateHealth()
    {
        return health / maxHealth;
    }

    public void DeductHealth(float deductHealth)
    {
        health -= deductHealth;
    }
    
    

    public void candyDamage(float candyDm)
    {
        health -= candyDm;
    }

    void Dead()
    {

        anim.SetTrigger("Dead");
        GetComponent<BoxCollider>().enabled = false;
        Destroy(gameObject, 1);
    }
    


}