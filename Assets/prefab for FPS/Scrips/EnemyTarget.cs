using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTarget : MonoBehaviour
{
    public float health = 100f;
    public static EnemyTarget instance;
    public GameObject enamyBugs;

    public void Awake()
    {
        if (instance == null)
            instance = this;
        /*else
        {

            Destroy(gameObject);
        }*/
    }

    public void TakeDamage(float amount)
    {
        health -= amount;
        if (health <=0)
        {
            Die();
        }
    }

    void Die()
    {
        Destroy(enamyBugs);
    }
}
