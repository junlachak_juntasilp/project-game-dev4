using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Scrips
{
    public class MainMenuController : MonoBehaviour
    {
      //  [SerializeField] Button startButton;
        [SerializeField] Button optionsButton; 
        [SerializeField] Button exitButton;
        
        // Start is called before the first frame update
        void Start()
        { /* 
            startButton.onClick.AddListener (
               delegate{StartButtonClick(startButton);}); */
            optionsButton.onClick.AddListener (
                delegate{OptionsButtonClick(optionsButton);}); 
            exitButton.onClick.AddListener (
                delegate{ExitButtonClick(exitButton);}); 
        }

        // Update is called once per frame
        void Update()
        {
        
        }

        public void StartButtonClick(Button button)
        {
            //SceneManager.LoadScene("intro");
        }

        public void OptionsButtonClick(Button button)
        {
            if (!GameApplicationManager.Instance.IsOptionMenuActive)
            {
                SceneManager.LoadScene("SceneOptions", LoadSceneMode.Additive);
                GameApplicationManager.Instance.IsOptionMenuActive = true;
            }
        }

        public void ExitButtonClick(Button button)
        {
            Debug.Log("Exit Game");
            Application.Quit();
        }

    }
}
