using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ATKall : MonoBehaviour
{
    public TextMeshProUGUI overallAtk;

    public static int atkDamage ;
    

    public int currentDmg ;
    // Start is called before the first frame update
    void Start()
    {
        atkDamage = currentDmg ;
    }

    // Update is called once per frame
    void Update()
    {
        overallAtk.text =  atkDamage.ToString();
    }

    /*private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "inventory_ATK")
        {
            
        }
    }*/
    public void  addAtk()
    {
        overallAtk.text = "พลังโจมตีทั้งหมด : " + atkDamage;
    }

    public static void currentDmgATK(int plusAtlamount)
    {
        atkDamage += plusAtlamount;
    }
}
