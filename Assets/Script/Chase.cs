using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chase : MonoBehaviour
{
    public Transform player;
    public Transform head;
    Animator anim;

    string state = "patrol";
    public GameObject[] waypoints;
    int currentWP = 0;
    float rotSpeed = 0.2f;
    public float speed = 1.5f;
    float accuracyWP = 5.0f;

   
    
    void Start()
    {
        anim = GetComponent<Animator>();
   
    }

    void Update()
    {
        Vector3 direction = player.position - this.transform.position;
        direction.z = 0;
        float angle = Vector3.Angle(direction, head.up);
        

        if (state == "patrol" && waypoints.Length > 0)
        {
            anim.SetBool("isIdle", false);
            anim.SetBool("iswalking", true);
            if (Vector3.Distance(waypoints[currentWP].transform.position, transform.position) < accuracyWP)
            {
                currentWP++;
                if(currentWP >= waypoints.Length)
                {
                    currentWP = 0;
                }
            }

            direction = waypoints[currentWP].transform.position - transform.position;
            this.transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction), rotSpeed * Time.deltaTime);
            this.transform.Translate(0, 0, Time.deltaTime * speed);
        }

        if(Vector3.Distance(player.position, this.transform.position) < 5 && (angle <10 || state == "pursuing"))
        {
            state = "pursuing";
            this.transform.rotation = Quaternion.Slerp(this.transform.rotation, Quaternion.LookRotation(direction), rotSpeed * Time.deltaTime);
            if(direction.magnitude > 5)
            {
                this.transform.Translate(0, 0, Time.deltaTime * speed);             
                anim.SetBool("iswalking", true);      
                anim.SetBool("isAttacking", false);
            }
            else
            {
                anim.SetBool("isAttacking", true);
                anim.SetBool("iswalking", false);
            }
        }
        else
        {
            anim.SetBool("iswalking", true);
            anim.SetBool("isAttacking", false);
            state = "patrol";
        }
    }
}
