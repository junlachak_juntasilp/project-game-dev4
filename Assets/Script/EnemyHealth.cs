using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyHealth : MonoBehaviour
{
    public float health;
    public float maxHealth;
    public bool EnemyDead = false;

    public GameObject healtBarUi;
    public Slider slider;

    private AudioSource audioSource;
    public AudioClip saw;

    void DamageEnemy(int damageAmmount)
    {

        health -= damageAmmount;

    }

    // Start is called before the first frame update
    void Start()
    {
        health = maxHealth;
        slider.value = calculateHealth();
    }

    // Update is called once per frame
    void Update()
    {
        slider.value = calculateHealth();

        if(health < maxHealth)
        {
            healtBarUi.SetActive(true);
        }

        if(health <= 0 && EnemyDead == false)
        {
            EnemyDead = true;
            Destroy(gameObject);
        }

        if(health > maxHealth)
        {
            health = maxHealth;
        }
    }
    
    float calculateHealth()
    {
        return health / maxHealth;
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.collider.transform.tag == "Bullet")
        {
            audioSource = GetComponent<AudioSource>();
            audioSource.clip = saw;
            audioSource.Play();
        }
    }
}
