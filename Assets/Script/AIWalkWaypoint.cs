using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIWalkWaypoint : MonoBehaviour
{
    public Transform[] waypoints;
    public int speed;

    private int wayintIndex;
    private float dist;

    private Animator anim;
    public Transform player;
    public Transform head;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        wayintIndex = 0;
        transform.LookAt(waypoints[wayintIndex].position);
    }

    // Update is called once per frame
    void Update()
    {
        anim.SetBool("Walk Forward", true);
        dist = Vector3.Distance(transform.position, waypoints[wayintIndex].position);        
        if (dist < 1f)
        {
            IncreaseIndex();
        }
        patrol();
    }

    void patrol()
    {
        transform.Translate(Vector3.forward * speed * Time.deltaTime);
    }

    void IncreaseIndex()
    {
        wayintIndex++;
        if(wayintIndex >= waypoints.Length)
        {
            wayintIndex = 0;
        }
        transform.LookAt(waypoints[wayintIndex].position);
    }
}
